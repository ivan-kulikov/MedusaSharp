﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedusaSharp.Loader.Pe.PeFormat
{
    /*
     * typedef struct _IMAGE_COFF_SYMBOLS_HEADER {
     *  DWORD NumberOfSymbols;
     *  DWORD LvaToFirstSymbol;
     *  DWORD NumberOfLinenumbers;
     *  DWORD LvaToFirstLinenumber;
     *  DWORD RvaToFirstByteOfCode;
     *  DWORD RvaToLastByteOfCode;
     *  DWORD RvaToFirstByteOfData;
     *  DWORD RvaToLastByteOfData;
     *  } IMAGE_COFF_SYMBOLS_HEADER, *PIMAGE_COFF_SYMBOLS_HEADER; 
     */

    /// <summary>
    /// DBG file COFF debug information header
    /// </summary>
    public struct IMAGE_COFF_SYMBOLS_HEADER
    {
        /// <summary>
        /// The number of symbols.
        /// </summary>
        public UInt32 NumberOfSymbols;
        /// <summary>
        /// The virtual address of the first symbol.
        /// </summary>
        public UInt32 LvaToFirstSymbol;
        /// <summary>
        /// The number of line-number entries.
        /// </summary>
        public UInt32 NumberOfLinenumbers;
        /// <summary>
        /// The virtual address of the first line-number entry.
        /// </summary>
        public UInt32 LvaToFirstLinenumber;
        /// <summary>
        /// The relative virtual address of the first byte of code.
        /// </summary>
        public UInt32 RvaToFirstByteOfCode;
        /// <summary>
        /// The relative virtual address of the last byte of code.
        /// </summary>
        public UInt32 RvaToLastByteOfCode;
        /// <summary>
        /// The relative virtual address of the first byte of data.
        /// </summary>
        public UInt32 RvaToFirstByteOfData;
        /// <summary>
        /// The relative virtual address of the last byte of data.
        /// </summary>
        public UInt32 RvaToLastByteOfData;

    }
}
