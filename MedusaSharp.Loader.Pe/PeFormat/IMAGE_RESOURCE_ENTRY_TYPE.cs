﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedusaSharp.Loader.Pe.PeFormat
{
    public enum IMAGE_RESOURCE_ENTRY_TYPE : ushort
    {
        PE_RT_CURSOR = 1,
        PE_RT_BITMAP = 2,
        PE_RT_ICON = 3,
        PE_RT_MENU = 4,
        PE_RT_DIALOG = 5,
        PE_RT_STRING = 6,
        PE_RT_FONTDIR = 7,
        PE_RT_FONT = 8,
        PE_RT_ACCELERATOR = 9,
        PE_RT_RCDATA = 10,
        PE_RT_MESSAGETABLE = 11,
        PE_RT_GROUP_CURSOR = 12,
        PE_RT_GROUP_ICON = 14,
        PE_RT_VERSION = 16,
        PE_RT_DLGINCLUDE = 17,
        PE_RT_PLUGPLAY = 19,
        PE_RT_VXD = 20,
        PE_RT_ANICURSOR = 21,
        PE_RT_ANIICON = 22,
        PE_RT_HTML = 23,
        PE_RT_MANIFEST = 24
    }
}
