﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MedusaSharp.Loader.Pe.PeFormat.Coff
{
    /*
Offset Size Field               Description
------ ---- ------------------- -----------------------------------------------
0       2   Machine             Number identifying type of target machine.
2       2   Number of Sections  Number of sections; indicates size of the
                                Section Table, which immediately follows
                                the headers.
4       4   Time/Date Stamp     Time and date the file was created.
8       4   Pointer to Symbol   Offset, within the COFF file, of the symbol
            Table               table.
12      4   Number of Symbols   Number of entries in the symbol table.
                                This data can be used in locating the
                                string table, which immediately follows
                                the symbol table.
16      2   Optional Header     Size of the optional header, which is
            Size                included for executable files but not
                                object files. An object file should have a
                                value of 0 here.
18      2   Characteristics     Flags indicating attributes of the file.
     */
    /// <summary>
    /// TODO add code
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct CoffHeader
    { 
    
    }
}
