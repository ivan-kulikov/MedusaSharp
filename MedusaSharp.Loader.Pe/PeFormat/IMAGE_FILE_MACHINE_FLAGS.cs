﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedusaSharp.Loader.Pe.PeFormat
{
    /// <summary>
    /// The architecture type of the computer. An image file can only be run on the
    /// specified computer or a system that emulates the specified computer. 
    /// </summary>
    [Flags]
    public enum IMAGE_FILE_MACHINE_FLAGS : ushort
    {
        /// <summary>
        /// 80386
        /// </summary>
        IMAGE_FILE_MACHINE_I386 = 0x014c,
        /// <summary>
        /// 80486
        /// </summary>
        IMAGE_FILE_MACHINE_I486 = 0x014D,
        /// <summary>
        /// 80586
        /// </summary>
        IMAGE_FILE_MACHINE_I586 = 0x014E,
        /// <summary>
        /// Intel Itanium
        /// </summary>
        IMAGE_FILE_MACHINE_IA64 = 0x0200,
        /// <summary>
        /// MIPS Mark I (R2000, R3000)
        /// </summary>
        IMAGE_FILE_MACHINE_R3000 = 0x0162,
        /// <summary>
        /// MIPS Mark II (R6000)
        /// </summary>
        IMAGE_FILE_MACHINE_R6000 = 0x0163,
        /// <summary>
        /// MIPS Mark III (R4000)
        /// </summary>
        IMAGE_FILE_MACHINE_R4000 = 0x0166,
        /// <summary>
        /// MIPS Mark IV (R10000)
        /// </summary>
        IMAGE_FILE_MACHINE_R10000 = 0x0168,
        /// <summary>
        /// MIPS little-endian WCE v2
        /// </summary>
        IMAGE_FILE_MACHINE_WCEMIPSV2 = 0x0169,
        /// <summary>
        /// MIPS16
        /// </summary>
        IMAGE_FILE_MACHINE_MIPS16 = 0x0266,
        /// <summary>
        /// MIPS with FPU
        /// </summary>
        IMAGE_FILE_MACHINE_MIPSFPU = 0x0366,
        /// <summary>
        /// MIPS16 with FPU
        /// </summary>
        IMAGE_FILE_MACHINE_MIPSFPU16 = 0x0466,
        /// <summary>
        /// DEC Alpha
        /// </summary>
        IMAGE_FILE_MACHINE_ALPHA = 0x0184,
        /// <summary>
        /// Dec Alpha 64-bit
        /// </summary>
        IMAGE_FILE_MACHINE_ALPHA64 = 0x0284,
        /// <summary>
        /// SH3
        /// </summary>
        IMAGE_FILE_MACHINE_SH3 = 0x01A2,
        /// <summary>
        /// SH3DSP
        /// </summary>
        IMAGE_FILE_MACHINE_SH3DSP = 0x01A3,
        /// <summary>
        /// SH3E
        /// </summary>
        IMAGE_FILE_MACHINE_SH3E = 0x01A4,
        /// <summary>
        /// SH4
        /// </summary>
        IMAGE_FILE_MACHINE_SH4 = 0x01A6,
        /// <summary>
        /// SH5
        /// </summary>
        IMAGE_FILE_MACHINE_SH5 = 0x01A8,
        /// <summary>
        /// ARM
        /// </summary>
        IMAGE_FILE_MACHINE_ARM = 0x01C0,
        /// <summary>
        /// ARM with Thumb
        /// </summary>
        IMAGE_FILE_MACHINE_ARMI = 0x01C2,
        /// <summary>
        /// ARMv7 (or higher) Thumb mode only
        /// </summary>
        IMAGE_FILE_MACHINE_ARMV7 = 0x01C4,
        /// <summary>
        /// Matsushita (Panasonic) AM33/MN10300
        /// </summary>
        IMAGE_FILE_MACHINE_AM33 = 0x01D3,
        /// <summary>
        /// PowerPC
        /// </summary>
        IMAGE_FILE_MACHINE_PPC = 0x01F0,
        /// <summary>
        /// PowerPC with floating-point
        /// </summary>
        IMAGE_FILE_MACHINE_PPCFP = 0x01F1,
        /// <summary>
        /// PowerPC Big-Endian
        /// </summary>
        IMAGE_FILE_MACHINE_PPCBE = 0x01F2,
        /// <summary>
        /// M32R little-endian. Processor Renesas M32R
        /// </summary>
        IMAGE_FILE_MACHINE_M32R = 0x9041,
        /// <summary>
        /// ARM EPOC
        /// </summary>
        IMAGE_FILE_MACHINE_EPOC = 0x0A00,
        /// <summary>
        /// AMD64 (x64)
        /// </summary>
        IMAGE_FILE_MACHINE_AMD64 = 0x8664,
        /// <summary>
        /// Motorola 68000 series
        /// </summary>
        IMAGE_FILE_MACHINE_M68K = 0x0268,
        /// <summary>
        /// EFI Bytecode
        /// </summary>
        IMAGE_FILE_MACHINE_EBC = 0x0EBC,
        /// <summary>
        /// ???
        /// </summary>
        IMAGE_FILE_MACHINE_CEF = 0x0CEF,
        /// <summary>
        /// ???
        /// </summary>
        IMAGE_FILE_MACHINE_CEE = 0xC0EE,
        /// <summary>
        /// TRICORE (Infineon)
        /// </summary>
        IMAGE_FILE_MACHINE_TRICORE = 0x0520

    }
}
