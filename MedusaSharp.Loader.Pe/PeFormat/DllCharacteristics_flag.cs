﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedusaSharp.Loader.Pe.PeFormat
{

    [Flags]
    public enum DllCharacteristicsType : ushort
    {
        /// <summary>
        /// Per-Process Library Initialization.
        /// </summary>
        IMAGE_DLLCHARACTERISTICS_PINIT = 0x0001,
        /// <summary>
        /// Per-Process Library Termination.
        /// </summary>
        IMAGE_DLLCHARACTERISTICS_PTERM = 0x0002,
        /// <summary>
        /// Per-Thread Library Initialization.
        /// </summary>
        IMAGE_DLLCHARACTERISTICS_TINIT = 0x0004,
        /// <summary>
        ///  Per-Thread Library Termination.
        /// </summary>
        IMAGE_DLLCHARACTERISTICS_TTERM = 0x0008,
        /// <summary>
        /// The DLL can be relocated at load time.
        /// </summary>
        IMAGE_DLLCHARACTERISTICS_DYNAMIC_BASE = 0x0040,
        /// <summary>
        /// Code integrity checks are forced.
        /// </summary>
        IMAGE_DLLCHARACTERISTICS_FORCE_INTEGRITY = 0x0080,
        /// <summary>
        /// The image is compatible with data execution prevention (DEP).
        /// </summary>
        IMAGE_DLLCHARACTERISTICS_NX = 0x0100,
        /// <summary>
        /// The image is isolation aware, but should not be isolated.
        /// </summary>
        IMAGE_DLLCHARACTERISTICS_NO_ISOLATION = 0x0200,
        /// <summary>
        /// The image does not use structured exception handling (SEH). No handlers can be called in this image.
        /// </summary>
        IMAGE_DLLCHARACTERISTICS_NO_SEH = 0x0400,
        /// <summary>
        /// Do not bind image
        /// </summary>
        IMAGE_DLLCHARACTERISTICS_NO_BIND = 0x0800,
        /// <summary>
        /// Driver is a WDM Driver
        /// </summary>
        IMAGE_DLLCHARACTERISTICS_WDM_DRV = 0x2000,
        /// <summary>
        /// Image is Terminal Server aware
        /// </summary>
        IMAGE_DLLCHARACTERISTICS_TSRVAWA = 0x8000
    }

}
