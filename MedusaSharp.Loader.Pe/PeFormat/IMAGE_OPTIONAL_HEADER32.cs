﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion
using MedusaSharp.Entries;
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MedusaSharp.Loader.Pe.PeFormat
{
    /*
     * 
     * typedef struct _IMAGE_OPTIONAL_HEADER {
     * WORD                 Magic;
     * BYTE                 MajorLinkerVersion;
     * BYTE                 MinorLinkerVersion;
     * DWORD                SizeOfCode;
     * DWORD                SizeOfInitializedData;
     * DWORD                SizeOfUninitializedData;
     * DWORD                AddressOfEntryPoint;
     * DWORD                BaseOfCode;
     * DWORD                BaseOfData;
     * DWORD                ImageBase;
     * DWORD                SectionAlignment;
     * DWORD                FileAlignment;
     * WORD                 MajorOperatingSystemVersion;
     * WORD                 MinorOperatingSystemVersion;
     * WORD                 MajorImageVersion;
     * WORD                 MinorImageVersion;
     * WORD                 MajorSubsystemVersion;
     * WORD                 MinorSubsystemVersion;
     * DWORD                Win32VersionValue;
     * DWORD                SizeOfImage;
     * DWORD                SizeOfHeaders;
     * DWORD                CheckSum;
     * WORD                 Subsystem;
     * WORD                 DllCharacteristics;
     * DWORD                SizeOfStackReserve;
     * DWORD                SizeOfStackCommit;
     * DWORD                SizeOfHeapReserve;
     * DWORD                SizeOfHeapCommit;
     * DWORD                LoaderFlags;
     * DWORD                NumberOfRvaAndSizes;
     * IMAGE_DATA_DIRECTORY DataDirectory[IMAGE_NUMBEROF_DIRECTORY_ENTRIES];
     * } IMAGE_OPTIONAL_HEADER, *PIMAGE_OPTIONAL_HEADER;
     */


    [StructLayout(LayoutKind.Explicit)]
    public struct IMAGE_OPTIONAL_HEADER32 : IDump
    {
        /// <summary>
        /// A signature WORD, identifying what type of header this is. The two most 
        /// common values are IMAGE_NT_OPTIONAL_HDR32_MAGIC 0x10b and 
        /// IMAGE_NT_OPTIONAL_HDR64_MAGIC 0x20b.
        /// </summary>
        [FieldOffset(0)]
        public MagicType Magic;
        /// <summary>
        /// The major version of the linker used to build this executable. For PE files
        /// from the Microsoft linker, this version number corresponds to the Visual 
        /// Studio version number (for example, version 6 for Visual Studio 6.0).
        /// </summary>
        [Description("The major version of the linker used to build this executable.")]
        [FieldOffset(2)]
        public byte MajorLinkerVersion;
        /// <summary>
        /// The minor version of the linker used to build this executable.
        /// </summary>
        [FieldOffset(3)]
        public byte MinorLinkerVersion;
        /// <summary>
        /// The combined total size of all sections with the IMAGE_SCN_CNT_CODE attribute.
        /// </summary>
        [FieldOffset(4)]
        public UInt32 SizeOfCode;
        /// <summary>
        /// The combined size of all initialized data sections.
        /// </summary>
        [FieldOffset(8)]
        public uint SizeOfInitializedData;
        /// <summary>
        /// The size of all sections with the uninitialized data attributes. This field 
        /// will often be 0, since the linker can append uninitialized data to the end
        /// of regular data sections.
        /// </summary>
        [FieldOffset(12)]
        public uint SizeOfUninitializedData;
        /// <summary>
        /// The RVA of the first code byte in the file that will be executed. For DLLs, 
        /// this entrypoint is called during process initialization and shutdown and 
        /// during thread creations/destructions. In most executables, this address 
        /// doesn't directly point to main, WinMain, or DllMain. Rather, it points to 
        /// runtime library code that calls the aforementioned functions. This field can 
        /// be set to 0 in DLLs, and none of the previous notifications will be received.
        /// The linker /NOENTRY switch sets this field to 0.
        /// </summary>
        [FieldOffset(16)]
        public uint AddressOfEntryPoint;
        /// <summary>
        /// The RVA of the first byte of code when loaded in memory.
        /// </summary>
        [FieldOffset(20)]
        public uint BaseOfCode;
        /// <summary>
        /// Theoretically, the RVA of the first byte of data when loaded into memory. 
        /// However, the values for this field are inconsistent with different versions 
        /// of the Microsoft linker. This field is not present in 64-bit executables.
        /// </summary>
        [FieldOffset(24)]
        public uint BaseOfData;
        // <summary>
        /// The preferred load address of this file in memory. The loader attempts to 
        /// load the PE file at this address if possible (that is, if nothing else 
        /// currently occupies that memory, it's aligned properly and at a legal address, 
        /// and so on). If the executable loads at this address, the loader can skip the 
        /// step of applying base relocations (described in Part 2 of this article). 
        /// For EXEs, the default ImageBase is 0x400000. For DLLs, it's 0x10000000. 
        /// The ImageBase can be set at link time with the /BASE switch, or later with the 
        /// REBASE utility.
        /// </summary>
        [FieldOffset(28)]
        public uint ImageBase;
        /// <summary>
        /// The alignment of sections when loaded into memory. The alignment must be 
        /// greater or equal to the file alignment field (mentioned next). The default 
        /// alignment is the page size of the target CPU. For user mode executables to 
        /// run under Windows 9x or Windows Me, the minimum alignment size is a page (4KB).
        /// This field can be set with the linker /ALIGN switch.
        /// </summary>
        [FieldOffset(32)]
        public uint SectionAlignment;
        /// <summary>
        /// The alignment of sections within the PE file. For x86 executables, this value 
        /// is usually either 0x200 or 0x1000. The default has changed with different 
        /// versions of the Microsoft linker. This value must be a power of 2, and if the 
        /// SectionAlignment is less than the CPU's page size, this field must match the 
        /// SectionAlignment. The linker switch /OPT:WIN98 sets the file alignment on x86 
        /// executables to 0x1000, while /OPT:NOWIN98 sets the alignment to 0x200.
        /// </summary>
        [FieldOffset(36)]
        public uint FileAlignment;
        /// <summary>
        /// The major version number of the required operating system. With the advent 
        /// of so many versions of Windows, this field has effectively become irrelevant.
        /// </summary>
        [FieldOffset(40)]
        public ushort MajorOperatingSystemVersion;
        /// <summary>
        /// The minor version number of the required OS.
        /// </summary>
        [FieldOffset(42)]
        public ushort MinorOperatingSystemVersion;
        /// <summary>
        /// The major version number of this file. Unused by the system and can be 0. 
        /// It can be set with the linker /VERSION switch.
        /// </summary>
        [FieldOffset(44)]
        public ushort MajorImageVersion;
        /// <summary>
        /// The minor version number of this file.
        /// </summary>
        [FieldOffset(46)]
        public ushort MinorImageVersion;
        /// <summary>
        /// The major version of the operating subsystem needed for this executable. 
        /// At one time, it was used to indicate that the newer Windows 95 or Windows NT 4.0 
        /// user interface was required, as opposed to older versions of the Windows NT 
        /// interface. Today, because of the proliferation of the various versions of 
        /// Windows, this field is effectively unused by the system and is typically set 
        /// to the value 4. Set with the linker /SUBSYSTEM switch.
        /// </summary>
        [FieldOffset(48)]
        public ushort MajorSubsystemVersion;
        /// <summary>
        /// The minor version of the operating subsystem needed for this executable.
        /// </summary>
        [FieldOffset(50)]
        public ushort MinorSubsystemVersion;
        /// <summary>
        /// Another field that never took off. Typically set to 0.
        /// </summary>
        [FieldOffset(52)]
        public uint Win32VersionValue;
        /// <summary>
        /// SizeOfImage contains the RVA that would be assigned to the section following 
        /// the last section if it existed. This is effectively the amount of memory that 
        /// the system needs to reserve when loading this file into memory. This field
        /// must be a multiple of the section alignment.
        /// </summary>
        [FieldOffset(56)]
        public uint SizeOfImage;
        /// <summary>
        /// The combined size of the MS-DOS header, PE headers, and section table. All of 
        /// these items will occur before any code or data sections in the PE file. The 
        /// value of this field is rounded up to a multiple of the file alignment. 
        /// </summary>
        [FieldOffset(60)]
        public uint SizeOfHeaders;
        /// <summary>
        /// The checksum of the image. The CheckSumMappedFile API in IMAGEHLP.DLL can 
        /// calculate this value. Checksums are required for kernel-mode drivers and some 
        /// system DLLs. Otherwise, this field can be 0. The checksum is placed in the 
        /// file when the /RELEASE linker switch is used.
        /// </summary>
        [FieldOffset(64)]
        public uint CheckSum;
        /// <summary>
        /// An enum value indicating what subsystem (user interface type) the executable 
        /// expects. This field is only important for EXEs.
        /// </summary>
        [FieldOffset(68)]
        public SubSystemType Subsystem;
        /// <summary>
        /// Flags indicating characteristics of this DLL.
        /// </summary>
        [FieldOffset(70)]
        public DllCharacteristicsType DllCharacteristics;
        /// <summary>
        /// In EXE files, the maximum size the initial thread in the process can grow to. 
        /// This is 1MB by default. Not all this memory is committed initially.
        /// </summary>
        [FieldOffset(72)]
        public uint SizeOfStackReserve;
        /// <summary>
        /// In EXE files, the amount of memory initially committed to the stack. 
        /// By default, this field is 4KB.
        /// </summary>
        [FieldOffset(76)]
        public uint SizeOfStackCommit;
        /// <summary>
        /// In EXE files, the initial reserved size of the default process heap. 
        /// This is 1MB by default. In current versions of Windows, however, 
        /// the heap can grow beyond this size without intervention by the user.
        /// </summary>
        [FieldOffset(80)]
        public uint SizeOfHeapReserve;
        /// <summary>
        /// In EXE files, the size of memory committed to the heap. 
        /// By default, this is 4KB.
        /// </summary>
        [FieldOffset(84)]
        public uint SizeOfHeapCommit;
        /// <summary>
        /// This is obsolete.
        /// </summary>
        [FieldOffset(88)]
        public uint LoaderFlags;
        /// <summary>
        ///  At the end of the IMAGE_NT_HEADERS structure is an array of 
        ///  IMAGE_DATA_DIRECTORY structures. This field contains the number of entries in
        ///  the array. This field has been 16 since the earliest releases of Windows NT.
        /// </summary>
        [FieldOffset(92)]
        public uint NumberOfRvaAndSizes;

        [FieldOffset(96)]
        public IMAGE_DATA_DIRECTORY ExportTable;

        [FieldOffset(104)]
        public IMAGE_DATA_DIRECTORY ImportTable;

        [FieldOffset(112)]
        public IMAGE_DATA_DIRECTORY ResourceTable;

        [FieldOffset(120)]
        public IMAGE_DATA_DIRECTORY ExceptionTable;

        [FieldOffset(128)]
        public IMAGE_DATA_DIRECTORY CertificateTable;

        [FieldOffset(136)]
        public IMAGE_DATA_DIRECTORY BaseRelocationTable;

        [FieldOffset(144)]
        public IMAGE_DATA_DIRECTORY Debug;

        [FieldOffset(152)]
        public IMAGE_DATA_DIRECTORY Architecture;

        [FieldOffset(160)]
        public IMAGE_DATA_DIRECTORY GlobalPtr;

        [FieldOffset(168)]
        public IMAGE_DATA_DIRECTORY TLSTable;

        [FieldOffset(176)]
        public IMAGE_DATA_DIRECTORY LoadConfigTable;

        [FieldOffset(184)]
        public IMAGE_DATA_DIRECTORY BoundImport;

        [FieldOffset(192)]
        public IMAGE_DATA_DIRECTORY IAT;

        [FieldOffset(200)]
        public IMAGE_DATA_DIRECTORY DelayImportDescriptor;

        [FieldOffset(208)]
        public IMAGE_DATA_DIRECTORY CLRRuntimeHeader;

        [FieldOffset(216)]
        public IMAGE_DATA_DIRECTORY Reserved;

        public string Dump()
        {
            string Output = string.Empty;
            Output += "IMAGE_OPTIONAL_HEADER32" + Environment.NewLine;
            Output += "AddressOfEntryPoint:= 0x" + AddressOfEntryPoint.ToString("x") + Environment.NewLine;


            return Output;
        }
    }
}
