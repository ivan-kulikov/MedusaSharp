﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedusaSharp.Loader.Pe.PeFormat
{

    [Flags]
    public enum IMAGE_DEBUG_TYPE_FLAG : uint
    {
        /// <summary>
        /// Unknown value, ignored by all tools.
        /// </summary>
        IMAGE_DEBUG_TYPE_UNKNOWN = 0,
        /// <summary>
        /// COFF debugging information (line numbers, symbol table, and string table). 
        /// This type of debugging information is also pointed to by fields in the file
        /// headers.
        /// </summary>
        IMAGE_DEBUG_TYPE_COFF = 1,
        /// <summary>
        /// CodeView debugging information. The format of the data block is 
        /// described by the CodeView 4.0 specification.
        /// </summary>
        IMAGE_DEBUG_TYPE_CODEVIEW = 2,
        /// <summary>
        /// Frame pointer omission (FPO) information. This information tells the debugger 
        /// how to interpret nonstandard stack frames, which use the EBP register for a 
        /// purpose other than as a frame pointer.
        /// </summary>
        IMAGE_DEBUG_TYPE_FPO = 3,
        /// <summary>
        /// Miscellaneous information.
        /// </summary>
        IMAGE_DEBUG_TYPE_MISC = 4,
        /// <summary>
        /// Exception information.
        /// </summary>
        IMAGE_DEBUG_TYPE_EXCEPTION = 5,
        /// <summary>
        /// Fixup information.
        /// </summary>
        IMAGE_DEBUG_TYPE_FIXUP = 6,
        /// <summary>
        /// 
        /// </summary>
        DBG_OMAP_TO_SRC = 7,
        /// <summary>
        /// 
        /// </summary>
        DBG_OMAP_FROM_SRC = 8,
        /// <summary>
        /// Borland debugging information.
        /// </summary>
        IMAGE_DEBUG_TYPE_BORLAND = 9,
        DBG_CLSID = 11
    }

}
