﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MedusaSharp.Loader.Pe.PeFormat
{
    /*
     * typedef struct _IMAGE_FILE_HEADER {  (size,offset(HEX))
     * WORD  Machine;           (size 2; offset 00)
     * WORD  NumberOfSections;  (size 2,offset 02)
     * DWORD TimeDateStamp;      (size 4,offset 04)
     * DWORD PointerToSymbolTable; (offset 08)
     * DWORD NumberOfSymbols;  (offset 0C)
     * WORD  SizeOfOptionalHeader; (offset 10)
     * WORD  Characteristics; (offset 12)
     *  IMAGE_FILE_HEADER, *PIMAGE_FILE_HEADER;
     */

    /// <summary>
    /// Represents the COFF header format.
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct IMAGE_FILE_HEADER
    {
        /// <summary>
        /// The architecture type of the computer. 
        /// An image file can only be run on the specified computer or a system that
        /// emulates the specified computer. This member can be one of the following values.
        /// </summary>
        [FieldOffset(0)]
        public UInt16 Machine;
        /// <summary>
        /// The number of sections. This indicates the size of the section table, 
        /// which immediately follows the headers. Note that the Windows loader 
        /// limits the number of sections to 96.
        /// </summary>
        [FieldOffset(2)]
        public UInt16 NumberOfSections;
        /// <summary>
        /// The low 32 bits of the time stamp of the image. This represents
        /// the date and time the image was created by the linker. The value is 
        /// represented in the number of seconds elapsed since midnight (00:00:00),
        /// January 1, 1970, Universal Coordinated Time, according to the system clock.
        /// </summary>
        [FieldOffset(4)]
        public UInt32 TimeDateStamp;
        /// <summary>
        /// The offset of the symbol table, in bytes, or zero if no COFF symbol table exists.
        /// </summary>
        [FieldOffset(8)]
        public UInt32 PointerToSymbolTable;
        /// <summary>
        /// The number of symbols in the symbol table.
        /// </summary>
        [FieldOffset(12)]
        public UInt32 NumberOfSymbols;
        /// <summary>
        /// The size of the optional header, in bytes. This value should be 0 for object files.
        /// </summary>
        [FieldOffset(16)]
        public UInt16 SizeOfOptionalHeader;
        /// <summary>
        /// The characteristics of the image. This member can be one or more 
        /// of the following values.
        /// </summary>
        [FieldOffset(18)]
        public UInt16 Characteristics;
    }
}
