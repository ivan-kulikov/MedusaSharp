﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion
using MedusaSharp.Entries;
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MedusaSharp.Loader.Pe.PeFormat
{
    /*
     * typedef struct _IMAGE_SECTION_HEADER {
     * BYTE  Name[IMAGE_SIZEOF_SHORT_NAME];
     * union {
     *   DWORD PhysicalAddress;
     *   DWORD VirtualSize;
     * } Misc;
     * DWORD VirtualAddress;
     * DWORD SizeOfRawData;
     * DWORD PointerToRawData;
     * DWORD PointerToRelocations;
     * DWORD PointerToLinenumbers;
     * WORD  NumberOfRelocations;
     * WORD  NumberOfLinenumbers;
     * DWORD Characteristics;
     * } IMAGE_SECTION_HEADER, *PIMAGE_SECTION_HEADER;
     */

    [StructLayout(LayoutKind.Explicit)]
    public struct IMAGE_SECTION_HEADER: IDump
    {
        /// <summary>
        /// An 8-byte, null-padded UTF-8 string. There is no terminating null character if 
        /// the string is exactly eight characters long. For longer names, this member 
        /// contains a forward slash (/) followed by an ASCII 
        /// representation of a decimal number that is an offset into the string table. Executable images 
        /// do not use a string table and do not support section names longer than eight characters.
        /// </summary>
        [FieldOffset(0)]
        [MarshalAs(UnmanagedType.ByValArray,SizeConst=8)]
        public byte[] Name;
        [FieldOffset(8)]
        public Misc Misc;
        /// <summary>
        /// The address of the first byte of the section when loaded into memory, 
        /// relative to the image base. For object files, this is the address of the 
        /// first byte before relocation is applied.
        /// </summary>
       [FieldOffset(12)]
        public UInt32 VirtualAddress;
        /// <summary>
        /// The size of the initialized data on disk, in bytes. This value must be a multiple 
        /// of the FileAlignment member of the IMAGE_OPTIONAL_HEADER structure. If this value is less 
        /// than the VirtualSize member, the remainder of the section is filled with zeroes. If 
        /// the section contains only uninitialized data, the member is zero.
        /// </summary>
        [FieldOffset(16)]
        public UInt32 SizeOfRawData;
        /// <summary>
        /// A file pointer to the first page within the COFF file. This value must be a multiple of the 
        /// FileAlignment member of the IMAGE_OPTIONAL_HEADER structure. If a section contains only 
        /// uninitialized data, set this member is zero.
        /// </summary>
        [FieldOffset(20)]
        public UInt32 PointerToRawData;
        /// <summary>
        /// A file pointer to the beginning of the relocation entries for the section. 
        /// If there are no relocations, this value is zero.
        /// </summary>
        [FieldOffset(24)]
        public UInt32 PointerToRelocations;
        /// <summary>
        /// A file pointer to the beginning of the line-number entries for the section. 
        /// If there are no COFF line numbers, this value is zero.
        /// </summary>
        [FieldOffset(28)]
        public UInt32 PointerToLinenumbers;
        /// <summary>
        /// The number of relocation entries for the section. 
        /// This value is zero for executable images.
        /// </summary>
        [FieldOffset(32)]
        public UInt16 NumberOfRelocations;
        /// <summary>
        /// The number of line-number entries for the section.
        /// </summary>
        [FieldOffset(34)]
        public UInt16 NumberOfLinenumbers;
        /// <summary>
        /// The characteristics of the image. The following values are defined. 
        /// </summary>
        [FieldOffset(36)]
        public SECTIONS_Characteristics_Flag Characteristics;

        public string Dump()
        {
            string Output = string.Empty;
            Output += "Misc.VirtualSize" + Misc.VirtualSize + Environment.NewLine;
            Output += "NumberOfLinenumbers:= " + NumberOfLinenumbers.ToString("x") + Environment.NewLine;
            Output += "VirtualAddress:= " + VirtualAddress.ToString("x") + Environment.NewLine;
            Output += "SizeOfRawData:= " + SizeOfRawData.ToString("x") + Environment.NewLine;
            Output += "PointerToRawData:= " + PointerToRawData.ToString("x") + Environment.NewLine;
            Output += "PointerToRelocations:= " + PointerToRelocations.ToString("x") + Environment.NewLine;
            Output += "PointerToLinenumbers:= " + PointerToLinenumbers.ToString("x") + Environment.NewLine;
            Output += "NumberOfRelocations:= " + NumberOfRelocations.ToString("x") + Environment.NewLine;

            return Output;
        }
    }
}
