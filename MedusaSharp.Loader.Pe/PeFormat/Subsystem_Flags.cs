﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedusaSharp.Loader.Pe.PeFormat
{
    [Flags]
    public enum SubSystemType : ushort
    {
        /// <summary>
        /// Unknown
        /// </summary>
        PES_UNKNOWN = 0,
        /// <summary>
        /// Native
        /// </summary>
        PES_NATIVE = 1,
        /// <summary>
        /// Windows GUI
        /// </summary>
        PES_WINGUI = 2,
        /// <summary>
        /// Windows Character
        /// </summary>
        PES_WINCHAR = 3,
        /// <summary>
        /// OS/2 Character
        /// </summary>
        PES_OS2CHAR = 5,
        /// <summary>
        /// Posix Character
        /// </summary>
        PES_POSIX = 7,
        /// <summary>
        /// image is a native Win9x driver
        /// </summary>
        PES_NAT9x = 8,
        /// <summary>
        /// Runs on Windows CE.
        /// </summary>
        PES_WINCE = 9,
        /// <summary>
        /// Extensible Firmware Interface (EFI) application.
        /// </summary>
        PES_EFI_APP = 10,
        /// <summary>
        /// EFI driver that provides boot services.
        /// </summary>
        PES_EFI_BDV = 11,
        /// <summary>
        /// EFI driver that provides runtime services.
        /// </summary>
        PES_EFI_RDV = 12,
        /// <summary>
        /// EFI ROM image
        /// </summary>
        PES_EFI_ROM = 13,
        /// <summary>
        /// Xbox system
        /// </summary>
        IMAGE_SUBSYSTEM_XBOX = 14,
        /// <summary>
        /// Windows Boot Application
        /// </summary>
        PES_BOOTAPP = 0x0010
    }
}
