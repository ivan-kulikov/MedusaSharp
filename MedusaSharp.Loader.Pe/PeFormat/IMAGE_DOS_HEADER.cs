﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace MedusaSharp.Loader.Pe.PeFormat
{
    [StructLayout(LayoutKind.Sequential)]
    public struct IMAGE_DOS_HEADER
    {
        /// <summary>
        /// Magic number ("MZ")  
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public char[] e_magic;
        /// <summary>
        /// Bytes on last page of file
        /// </summary>
        [Description("Bytes on last page of file")]
        public UInt16 e_cblp;
        /// <summary>
        /// Pages in file
        /// </summary>
        [Description("Pages in file")]
        public UInt16 e_cp;
        /// <summary>
        /// Relocations
        /// </summary>
        public UInt16 e_crlc;
        /// <summary>
        /// Size of header in paragraphs
        /// </summary>
        public UInt16 e_cparhdr;
        /// <summary>
        /// Minimum extra paragraphs needed
        /// </summary>
        public UInt16 e_minalloc;
        /// <summary>
        /// Maximum extra paragraphs needed
        /// </summary>
        public UInt16 e_maxalloc;
        /// <summary>
        /// Initial (relative) SS value
        /// </summary>
        public UInt16 e_ss;
        /// <summary>
        /// Initial SP value
        /// </summary>
        public UInt16 e_sp;
        /// <summary>
        /// Checksum
        /// </summary>
        public UInt16 e_csum;
        /// <summary>
        /// Initial IP value
        /// </summary>
        public UInt16 e_ip;
        /// <summary>
        /// Initial (relative) CS value
        /// </summary>
        public UInt16 e_cs;
        /// <summary>
        /// Address of relocation table
        /// </summary>
        public UInt16 e_lfarlc;
        /// <summary>
        /// Overlay number
        /// </summary>
        public UInt16 e_ovno;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        /// <summary>
        /// Reserved words
        /// </summary>
        public UInt16[] e_res1;
        /// <summary>
        /// OEM identifier (for e_oeminfo)
        /// </summary>
        public UInt16 e_oemid;
        /// <summary>
        /// OEM info; e_oemid specific
        /// </summary>
        public UInt16 e_oeminfo;
        /// <summary>
        /// Reserved words
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public UInt16[] e_res2;
        /// <summary>
        /// File address of new exe header
        /// </summary>
        public UInt32 e_lfanew;

        private string _e_magic
        {
            get { return new string(e_magic); }
        }

        public bool isValid
        {
            get { return _e_magic == "MZ"; }
        }
    }
}
