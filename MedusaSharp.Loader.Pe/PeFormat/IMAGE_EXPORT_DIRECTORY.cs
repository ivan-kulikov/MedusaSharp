﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MedusaSharp.Loader.Pe.PeFormat
{
    /*
    uint32 flags;     // Currently set to zero.
  qtime32_t datetime; // Time/Date the export data was created.
  uint16 major;     // A user settable major/minor version number.
  uint16 minor;
  uint32 dllname;   // Relative Virtual Address of the Dll asciiz Name.
                    // This is the address relative to the Image Base.
  uint32 ordbase;   // First valid exported ordinal. This field specifies
                    // the starting ordinal number for the export
                    // address table for this image. Normally set to 1.
  uint32 naddrs;    // Indicates number of entries in the Export Address
                    // Table.
  uint32 nnames;    // This indicates the number of entries in the Name
                    // Ptr Table (and parallel Ordinal Table).
  uint32 adrtab;    // Relative Virtual Address of the Export Address
                    // Table. This address is relative to the Image Base.
  uint32 namtab;    // Relative Virtual Address of the Export Name Table
                    // Pointers. This address is relative to the
                    // beginning of the Image Base. This table is an
                    // array of RVA's with # NAMES entries.
  uint32 ordtab;    // Relative Virtual Address of Export Ordinals
                    // Table Entry. This address is relative to the
                    // beginning of the Image Base.
    */
    [StructLayout(LayoutKind.Explicit)]
    public struct IMAGE_EXPORT_DIRECTORY
    {
        [FieldOffset(0)]
        UInt32 flags;

    }
}
