﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion
using MedusaSharp.Core;
using MedusaSharp.Core.PluginsSystem;
using Mono.Addins;
using NUnit.Framework;

namespace MedusaSharp.Tests.PluginSystemTest
{
    [TestFixture]
    public class PluginManagerTests
    {
        /// <summary>
        /// Test for MonoAddin
        /// </summary>
         [Test]
         public void _01_MonoAddinsTestWithHelloPlugin()
         {
             AddinManager.Initialize();
             AddinManager.Registry.Update(null);
             System.Console.WriteLine("Registry.GetAddinRoots()");
             foreach (Addin addin in AddinManager.Registry.GetAddinRoots())
             {
                 System.Console.WriteLine("addin.Namespace ", addin.Namespace);
                 System.Console.WriteLine("addin.Version ", addin.Version);
                 System.Console.WriteLine("addin.Name ", addin.Name);
                 System.Console.WriteLine("addin.Enabled: " + addin.Enabled);
                 System.Console.WriteLine("addin.Description: " + addin.Description);
                 System.Console.WriteLine("plugin id " + addin.Id);
                 System.Console.WriteLine(addin.AddinFile);
             }
             System.Console.WriteLine("Registry.GetAddins()");
             foreach (Addin addin in AddinManager.Registry.GetAddins())
             {
                 System.Console.WriteLine("------------------------");
                 System.Console.WriteLine("addin.Namespace ", addin.Namespace);
                 System.Console.WriteLine("addin.Version ", addin.Version);
                 System.Console.WriteLine("addin.Name ", addin.Name);
                 System.Console.WriteLine("addin.Enabled: " + addin.Enabled);
                 System.Console.WriteLine("addin.Description: " + addin.Description);
                 System.Console.WriteLine("plugin id " + addin.Id);
                 System.Console.WriteLine("plugin is SupportsVersion 0.0.0.1?  " + addin.SupportsVersion("0.0.0.1"));
                 System.Console.WriteLine(addin.AddinFile);
                 System.Console.WriteLine("------------------------");
             }
             System.Console.WriteLine("AddinManager.Registry.RegistryPath");
             System.Console.WriteLine(AddinManager.Registry.RegistryPath);
             System.Console.WriteLine("GetLoaders: ");

             foreach (ILoader loader in AddinManager.GetExtensionObjects(typeof(ILoader)))
             {
                 System.Console.WriteLine(" loader.GetName: " + loader.GetName);
                 System.Console.WriteLine(" loader.IsCompatible: " + loader.IsCompatible(null));
             }

             
           
         }
    }
}
