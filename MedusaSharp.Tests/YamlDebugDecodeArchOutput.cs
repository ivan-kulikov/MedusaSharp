﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion
using MedusaSharp.Entries;
using MedusaSharp.Entries.ArchitectureEntries;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace MedusaSharp.Tests
{
    [Ignore("Disabled")]
    [TestFixture]
    public class YamlDebugDecodeArchOutput
    {
        // developing yaml format for universal disambler...

        [Test]
        [Ignore("Disabled")]
        public void _01_YamlDebugdeserializex86()
        {
            var deserializer = new Deserializer(namingConvention: new CamelCaseNamingConvention());
        }

        [Ignore("Disabled")]
        [Test]
        public void YamlDebugserializeArchCreateFileAvr8()
        {
            Stream avr8yamlFile = File.Open(TestsGlobalConsts.GetPathToArchYamlFile("avr8"), FileMode.Create);
            avr8yamlFile.Close();
            TextWriter writer = File.CreateText(TestsGlobalConsts.GetPathToArchYamlFile("avr8"));
            var architecture = new Architecture();
            architecture.Name = "avr8";

            Instruction instructionNop = new Instruction();

            /// 

            
            var serializer = new Serializer(namingConvention: new CamelCaseNamingConvention());
            serializer.Serialize(writer, architecture);
            writer.Close();
        }
        [Ignore("Disabled")]
        [Test]
        public void YamlDebugserializeArchCreateFilex86()
        {
            Stream avr8yamlFile = File.Open(TestsGlobalConsts.GetPathToArchYamlFile("x86"), FileMode.Create);
            avr8yamlFile.Close();
            TextWriter writer = File.CreateText(TestsGlobalConsts.GetPathToArchYamlFile("x86"));
            var architecture = new Architecture();
            architecture.Name = "x86";
            /// Instruction Nop ///
            Instruction instructionNop = new Instruction();
            instructionNop.Name = "nop";
            /// Instruction Nop ///
            /// 
            /// Instruction push ///
            Instruction instructionPush = new Instruction();
            instructionPush.Name = "push";
            instructionPush.Description = "";
            architecture.Instructions.Add(instructionPush);
            /// Instruction push ///

            architecture.Instructions.Add(instructionNop);
            var serializer = new Serializer(namingConvention: new CamelCaseNamingConvention());
            serializer.Serialize(writer, architecture);
            writer.Close();
        }
        [Ignore("Disabled")]
        [Test]
        public void YamlDebugserializeArchCreateFilez80()
        {
            Stream avr8yamlFile = File.Open(TestsGlobalConsts.GetPathToArchYamlFile("avr8"), FileMode.Create);
            avr8yamlFile.Close();
            TextWriter writer = File.CreateText(TestsGlobalConsts.GetPathToArchYamlFile("avr8"));
            var architecture = new Architecture();
            architecture.Name = "z80";

            Instruction instructionNop = new Instruction();
            instructionNop.Name = "nop";
            /// 


            var serializer = new Serializer(namingConvention: new CamelCaseNamingConvention());
            serializer.Serialize(writer, architecture);
            writer.Close();
        }



    }
}
