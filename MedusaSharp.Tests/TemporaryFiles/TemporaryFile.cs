﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MedusaSharp.Tests.TemporaryFiles
{
    public class TemporaryFile : IDisposable
    {
        public TemporaryFile(string path,bool OpenTemporaryStream)
        {
            if (path == null)
                throw new ArgumentNullException("path");

            Path = path;

            if (OpenTemporaryStream)
            {
                TemporaryStream = new FileStream(path, FileMode.Create);
            }
            else
            {
                FileStream file = File.Create(path);
                file.Close();
            }
        }

        public Stream TemporaryStream { get; private set; }
        public string Path { get; private set; }
        public override string ToString()
        {
            return Path;
        }
        private bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {

            if (!_disposed)
            {
                if (disposing)
                {
                    if (TemporaryStream!=null) TemporaryStream.Dispose();
                    File.Delete(Path);
                }

                _disposed = true;
            }
        }

        ~TemporaryFile()
        {
            Dispose(false);
        }

    }

}
