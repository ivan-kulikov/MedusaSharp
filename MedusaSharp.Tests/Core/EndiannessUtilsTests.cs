﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion
using MedusaSharp.Core;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedusaSharp.Tests.Core
{
    [TestFixture]
    public class EndiannessUtilsTests
    {
        [Test]
        public void UInt16ReverseBytesTest1()
        {
            if (EndiannessUtils.IsBigEndianArch())
            {
                Assert.Ignore("little indian on my machine...Need machine with big indian");
            }
            //little indian on my machine(Hex:5 binary :00000000101)
            UInt16 u = 5;
            Console.WriteLine("UInt16 is little indian");
            Console.WriteLine("UInt16 Hex:" + u.ToString("X"));
            Console.WriteLine("UInt16:" + u.ToString(""));
            Console.WriteLine("UInt16 binary :" + Convert.ToString(u, 2));
            Console.WriteLine("---------------------------------------------");
            /*
             UInt16 is ReverseBytes now
             UInt16 Hex:500
             UInt16:1280
             UInt16 binary :10100000000
             */
            UInt16 newBigIndian = EndiannessUtils.ReverseBytes(u);
            Console.WriteLine("UInt16 is ReverseBytes now");
            Console.WriteLine("UInt16 Hex:" + newBigIndian.ToString("X"));
            Console.WriteLine("UInt16:" + newBigIndian.ToString(""));
            Console.WriteLine("UInt16 binary :" + Convert.ToString(newBigIndian, 2));
        }

        [Test]
        public void UInt16ReverseBytesTest2()
        {
            Assert.AreEqual(0xBBAA, EndiannessUtils.ReverseBytes((ushort)0xAABB));
        }
    }
}
