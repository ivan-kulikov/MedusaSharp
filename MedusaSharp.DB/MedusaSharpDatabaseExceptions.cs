﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedusaSharp.DB
{
    public class MedusaSharpDatabaseException : Exception
    {
        public MedusaSharpDatabaseException() : base() { }
        public MedusaSharpDatabaseException(string message) : base(message) { }
        public MedusaSharpDatabaseException(string message, System.Exception inner) : base(message, inner) { }
    }

    /// <summary>
    /// Database is corrupted
    /// </summary>
    public class MedusaSharpInvalidDatabaseException : MedusaSharpDatabaseException
    {
        public MedusaSharpInvalidDatabaseException() : base() { }
        public MedusaSharpInvalidDatabaseException(string message) : base(message) { }
        public MedusaSharpInvalidDatabaseException(string message, System.Exception inner) : base(message, inner) { }
    }

    public class MedusaSharpDatabaseYetExitstsException : MedusaSharpDatabaseException
    {
        public MedusaSharpDatabaseYetExitstsException() : base() { }
        public MedusaSharpDatabaseYetExitstsException(string message) : base(message) { }
        public MedusaSharpDatabaseYetExitstsException(string message, System.Exception inner) : base(message, inner) { }
    }
    /// <summary>
    /// Exception MedusaSharp Database Not Opened
    /// </summary>
    public class MedusaSharpDatabaseNotOpenedException : MedusaSharpDatabaseException
    {
        public MedusaSharpDatabaseNotOpenedException() : base() { }
        public MedusaSharpDatabaseNotOpenedException(string message) : base(message) { }
        public MedusaSharpDatabaseNotOpenedException(string message, System.Exception inner) : base(message, inner) { }
    }

    public class MedusaSharpDatabaseFileNotFoundException : MedusaSharpDatabaseException
    {
        public MedusaSharpDatabaseFileNotFoundException() : base() { }
        public MedusaSharpDatabaseFileNotFoundException(string message) : base(message) { }
        public MedusaSharpDatabaseFileNotFoundException(string message, System.Exception inner) : base(message, inner) { }
    }

    public class MedusaSharpDatabaseYetOpeningException : MedusaSharpDatabaseException
    {
        public MedusaSharpDatabaseYetOpeningException() : base() { }
        public MedusaSharpDatabaseYetOpeningException(string message) : base(message) { }
        public MedusaSharpDatabaseYetOpeningException(string message, System.Exception inner) : base(message, inner) { }
    }



}
