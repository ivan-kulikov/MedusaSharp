﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using MedusaSharp.Entries;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MedusaSharp.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class MedusaSharpBinaryReader : BinaryReader
    {
        public MedusaSharpBinaryReader(Stream input)
            : base(input)
        { }

        public UInt64 EReadUInt64(UInt64 Position)
        {
            throw new Exception("make it!!!");
        }

        public Int64 EReadSInt64(UInt64 Position)
        {
            throw new Exception("make it!!!");
        }

        public UInt32 EReadUInt32(UInt64 Position)
        {

            throw new Exception("make it!!!");
        }

        public Int32 EReadSInt32(UInt64 Position)
        {
            throw new Exception("make it!!!");
        }

        public Int16 EReadSInt16(UInt64 Position)
        {
            throw new Exception("make it!!!");
        }

        public UInt16 EReadUInt16(UInt64 Position)
        {
            throw new Exception("make it!!!");
        }

        public sbyte EReadSInt8(UInt64 Position)
        {
            throw new Exception("make it!!!");
        }
        /// <summary>
        /// Read UInt8 and Swap if necessary
        /// </summary>
        public byte EReadUInt8(UInt64 Position)
        {
            throw new Exception("make it!!!");
        }

        public T ERead<T>()
        {
            throw new Exception("make it!!!");
        }

        public T Read<T>()
        {
            int iSize = Marshal.SizeOf(typeof(T));
            byte[] bytes = this.ReadBytes(iSize);
            return FromArray<T>(bytes);
        }

        private static T FromArray<T>(byte[] array)
        {
            int iSize = Marshal.SizeOf(typeof(T));
            if (array.Length != iSize) throw new Exception("Wrong size of array");
            IntPtr ptr = IntPtr.Zero;
            T result = default(T);
            try
            {
                ptr = Marshal.AllocHGlobal(iSize);
                Marshal.Copy(array, 0, ptr, iSize);
                result = (T)Marshal.PtrToStructure(ptr, typeof(T));
            }
            finally
            {
                Marshal.FreeHGlobal(ptr);
            }
            return result;
        }
    }
}
