﻿using Mono.Addins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedusaSharp.Core.PluginsSystem
{
    public class PluginManager
    {
        protected PluginManager()
        {
            AddinManager.Initialize();
            AddinManager.Registry.Update();
        }
   
        public void DisableAllPlugins(){
            
        }
    }

    public class PluginManagerSingleton : PluginManager
    {
        private static readonly PluginManagerSingleton instance = new PluginManagerSingleton();
        public static PluginManagerSingleton Instance
        {
            get { return instance; }
        }
        protected PluginManagerSingleton()
        {
            
        }
    }
}
