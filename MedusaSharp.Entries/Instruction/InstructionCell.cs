﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedusaSharp.Entries
{
    /// <summary>
    /// InstructionCell is a Cell which handles an instruction for any Architecture
    /// </summary>
    public class InstructionCell : Cell
    {
        /// <summary>
        /// This string holds the instruction name ("call", "lsl", ...)
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// This integer holds the current opcode (ARM_Ldr, GB_Swap, ...)
        /// </summary>
        // TODO is U64????
        public UInt32 Opcd { get; set; }
        /// <summary>
        /// This integer holds prefix flag (REP, LOCK, ...) 
        /// </summary>
        public UInt32 Prefix { get; set; }


        /// <summary>
        /// InstructionCell construction
        /// </summary>
        /// <param name="pName"> Name is the name of the instruction </param>
        /// <param name="Opcode"></param>
        /// <param name="Length"></param>
        public InstructionCell(string pName, UInt32 pOpcode, UInt16 pLength)
        {
            this.Name = pName;
            this.Opcd = pOpcode;
            this.Length = pLength;
            this.Type = CellType.InstructionType;
            this.SubType = (byte)InstructionType.NoneType;
        }

        public override string ToString()
        {
            //string Res = string.Format("mnem: ", Name, Opcd, pLength);
            return string.Empty;
        }


    }
}
