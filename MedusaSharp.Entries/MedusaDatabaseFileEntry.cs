﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using ProtoBuf;
namespace MedusaSharp.Entries
{
    [ProtoContract]
    public class MedusaDatabaseFiles
    {
        /// <summary>
        /// File name
        /// </summary>
        [ProtoMember(1)]
        public string FileName { get; set; }
        /// <summary>
        /// File Description
        /// </summary>
        [ProtoMember(2)]
        public string FileDescription { get; set; }
        /// <summary>
        /// File 
        /// </summary>
        [ProtoMember(3)]
        public byte[] File { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [ProtoMember(4)]
        public FileType fileType { get; set; }
        [ProtoMember(5)]
        public string Sha1FileHash { get; set; }
    }
}
