﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedusaSharp.Entries
{
    /// <summary>
    /// This class holds an address in a generic way.
    /// </summary>
    [ProtoContract]
    public class Address
    {
        /// <summary>
        /// AddressType defines the kind of address.
        /// </summary>
        [ProtoMember(1)]
        public AddressType AdressType { get; set; }
        /// <summary>
        /// Base is the base of this address.
        /// </summary>
        [ProtoMember(2)]
        public UInt16 Base { get; set; }
        /// <summary>
        /// Offset is the offset of this address.
        /// </summary>
        public UInt64 Offset
        {
            get
            {
                return m_Offset;
            }
            set
            {
                m_Offset = value;
                SanitizeOffset();
            }
        }

        [ProtoMember(3)]
        private UInt64 m_Offset;

        /// <summary>
        /// BaseSize defines the base size in bits.
        /// </summary>
        [ProtoMember(4)]
        public Byte BaseSize { get; private set; }
        /// <summary>
        /// OffsetSize defines the offset size in bits.
        /// </summary>
        [ProtoMember(5)]
        public Byte OffsetSize { get; private set; }

        public Address()
        {
            AdressType = AddressType.UnknownType;
            BaseSize = 16;
            Offset = 0x0;
            OffsetSize = 64;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"> defines the kind of address. </param>
        /// <param name="baseAdr"> is the base of this address. </param>
        /// <param name="offset"> is the offset of this address. </param>
        /// <param name="basesize"> defines the base size in bits. </param>
        /// <param name="offsetsize"> defines the offset size in bits. </param>
        public Address(AddressType type, UInt16 baseAdr, UInt64 offset, Byte basesize, Byte offsetsize)
        {
            AdressType = type;
            Base = baseAdr;
            Offset = offset;
            BaseSize = basesize;
            OffsetSize = offsetsize;
            SanitizeOffset();
        }

        public Address(UInt16 baseadr, UInt64 offset, Byte basesize, Byte offsetsize)
        {
            AdressType = AddressType.FlatType;
            Base = baseadr;
            Offset = offset;
            BaseSize = basesize;
            OffsetSize = offsetsize;
            SanitizeOffset();
        }

        public Address(AddressType type, UInt16 baseadr, UInt64 offset)
        {
            AdressType = type;
            Base = baseadr;
            Offset = offset;
            BaseSize = 16;
            OffsetSize = 64;
        }

        public Address(UInt16 baseadr, UInt64 offset)
        {
            AdressType = AddressType.FlatType;
            Base = baseadr;
            Offset = offset;
            BaseSize = 16;
            OffsetSize = 64;
        }

        public Address(UInt64 offset, AddressType type)
        {
            AdressType = type;
            Base = 0x0;
            Offset = offset;
            BaseSize = 16;
            OffsetSize = 64;
        }

        public Address(UInt64 offset)
        {
            AdressType = AddressType.FlatType;
            Base = 0x0;
            Offset = offset;
            BaseSize = 16;
            OffsetSize = 64;
        }

        // TODO Address(std::string const& rAddrName);

        /// <summary>
        /// This method allows to mask an offset accordingly the current offset size.
        /// </summary>
        public UInt64 SanitizeOffset(UInt64 Offset)
        {
            switch (OffsetSize)
            {
                case 8: return Offset & 0xff;
                case 16: return Offset & 0xffff;
                case 32: return Offset & 0xffffffff;
                case 64: return Offset & 0xffffffffffffffff;
                default: return Offset;
            }
        }

        protected void SanitizeOffset()
        {
            SanitizeOffset(Offset);
        }

        public bool IsBetween(UInt64 Size, UInt64 Off)
        {
            return Off >= Offset && Off < Offset + Size;
        }

        public bool IsBetween(UInt64 Size, Address Addr)
        {
            //if (AdressType != Addr.AdressType)
            //  return false;
            if (Addr.AdressType != AddressType.UnknownType && Addr.Base != Base)
                return false;
            return Addr.Offset >= Offset && Addr.Offset < Offset + Size;
        }
        /// <summary>
        /// This method converts the current address to a string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string oss = string.Empty;
            if (AdressType != AddressType.FlatType && AdressType != AddressType.UnknownType)
            {
                oss += Base.ToString("X") + ":";
            }
            oss += Offset.ToString("X");
            return oss;
        }

        /// <summary>
        /// This method returns true if base and offset are equal.
        /// </summary>
        public static bool operator ==(Address addr1, Address addr2)
        {
            if ((addr1.Base == addr2.Base) && (addr1.Offset == addr2.Offset))
                return true;
            return false;
        }

        public static bool operator !=(Address addr1, Address addr2)
        {
            if ((addr1.Base != addr2.Base) || (addr1.Offset != addr2.Offset))
                return true;
            return false;
        }        

        /// <summary>
        /// This method returns true if both base and offset are superior or equal to rAddr.
        /// </summary>
        public static bool operator >=(Address addr1, Address addr2)
        {
            if (addr1.Base > addr2.Base)
                return true;
            else if (addr1.Base == addr2.Base)
                return addr1.Offset >= addr2.Offset;
            else
                return false;
        }

        public static bool operator <=(Address addr1, Address addr2)
        {
            if (addr1.Base < addr2.Base)
                return true;
            else if (addr1.Base == addr2.Base)
                return addr1.Offset <= addr2.Offset;
            else
                return false;
        }


        public static bool operator <(Address addr1, Address addr2)
        {
            if (addr1.Base < addr2.Base)
                return true;
            else if (addr1.Base == addr2.Base)
                return addr1.Offset < addr2.Offset;
            else
                return false;
        }

        /// <summary>
        /// This method returns true if both base and offset are superior to addr2.
        /// </summary>
        public static bool operator >(Address addr1, Address addr2)
        {
            if (addr1.Base > addr2.Base)
                return true;
            else if (addr1.Base == addr2.Base)
                return addr1.Offset > addr2.Offset;
            else
                return false;
        }

        /// <summary>
        /// This method returns an address where its offset is the current offset plus Off.
        /// </summary>
        public static Address operator +(Address addr1, UInt64 offsetplus)
        {
            Address Res = new Address(addr1.AdressType, addr1.Base, addr1.SanitizeOffset(addr1.Offset + offsetplus), addr1.BaseSize, addr1.OffsetSize);
            return Res;
        }

        // TODO Address Hash...

        //std::string Address::Dump(void) const
    }
}
