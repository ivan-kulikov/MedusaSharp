﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedusaSharp.Entries
{
    [Flags]
    /// <summary>
    /// Label type
    /// </summary>
    public enum LabelType
    {
        /// <summary>
        /// Undefined type.
        /// </summary>
        Unknown = 0x0000,
        /// <summary>
        /// This label contains code.
        /// </summary>
        Code = 0x0001,
        /// <summary>
        /// This label contains a function
        /// </summary>
        Function = 0x0002,
        /// <summary>
        /// This label contains data.
        /// </summary>
        Data = 0x0003,
        /// <summary>
        /// This label contains a string.
        /// </summary>
        String = 0x0004,
        CellMask = 0x000f,
        /// <summary>
        /// This label is imported.
        /// </summary>
        Imported = 0x0010,
        /// <summary>
        /// This label is exported.
        /// </summary>
        Exported = 0x0020,
        /// <summary>
        /// This label is global.
        /// </summary>
        Global = 0x0030,
        /// <summary>
        /// This label is local.
        /// </summary>
        Local = 0x0040,
        AccessMask = 0x00f0,
        /// <summary>
        /// This label is auto-generated
        /// </summary>
        AutoGenerated = 0x0100,
    };
}
