﻿#region copyright
/*
 * Copyright (C) 2011-2013 François Lalande, François-Xavier Oxeda, Édouard Fajnzilberg and Kévin Szkudłapski.
 * Copyright (C) 2015 Иван Куликов (Ivan Kulikov)
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#endregion

using BizArk.Core;
using BizArk.Core.CmdLine;
using MedusaSharp.Core;
using MedusaSharp.DB;
using NLog;
using System;
using System.ComponentModel;
namespace MedusaSharp.ConsoleApp
{
    [CmdLineOptions(ArgumentPrefix = "--", Description = "Medusa Sharp Console UI")]
    public class MyCmdLine
        : CmdLineObject
    {
        public MyCmdLine()
        {
            File = String.Empty;
            Database = String.Empty;
            RewriteDatabase = false;
        }

        [CmdLineArg(Alias = "F", Usage = "File", ShowInUsage = DefaultBoolean.True)]
        [System.ComponentModel.Description("File for analyse")]
        public string File { get; set; }

        [CmdLineArg(Alias = "D", Usage = "Database", ShowInUsage = DefaultBoolean.True, Required = true)]
        [System.ComponentModel.Description("Medusa Sharp Database")]
        public string Database { get; set; }

        [CmdLineArg(Alias = "R", Usage = "Rewrite", ShowInUsage = DefaultBoolean.True, Required = false)]
        [System.ComponentModel.Description("Rewrite exists database")]
        public bool RewriteDatabase { get; set; }
    }

    class MedusaSharpConsole
    {
        private static Logger log;
        static void Main(string[] args)
        {
            Console.WriteLine("Attach now lol ");
            Console.ReadLine();

            log = LogManager.GetCurrentClassLogger();
            try
            {
                NLogHeaderWrite();
                ConsoleApplication.RunProgram<MyCmdLine>(Start);
            }
            catch (Exception e)
            {
                log.Fatal(e);
                throw e;
            }
        }

        private static void Start(MyCmdLine args)
        {
            if (args.Help || !args.IsValid())
            {
                Console.WriteLine(args.GetHelpText(Console.WindowWidth));
                return;
            }

            MedusaSharpMain medusaSharpMain = new MedusaSharpMain();
            try
            {
                if (args.File == String.Empty)
                {
                    medusaSharpMain.OpenDocument(args.Database);
                }
                else
                {
                    medusaSharpMain.NewDocument(args.File, args.Database, args.RewriteDatabase,true);

                }
            }
            catch (MedusaSharpDatabaseFileNotFoundException exp)
            {
                Console.WriteLine(exp.Message);
                return;
            }
            catch (MedusaSharpInvalidDatabaseException exp)
            {
                Console.WriteLine(exp.Message);
                return;
            }
            catch (MedusaSharpDatabaseYetExitstsException)
            {
                Console.WriteLine("Database file exists!!(For rewrite --R true)");
            }
        }

        private static void NLogHeaderWrite()
        {
            log.Info("----------------------------------------");
            log.Info("Program Started.");
            OperatingSystem os = Environment.OSVersion;
            Version ver = os.Version;
            log.Info("Operating System: {0} ({1})", os.VersionString, ver.ToString());
            log.Info(Environment.CurrentDirectory);
            Console.Title = "Medusa Sharp Console UI";
        }





    }
}