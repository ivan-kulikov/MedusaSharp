﻿namespace Tech.DynamicCoding
{
    public interface IDynamicCode<out TResult>
    {
         TResult Execute(params object[] parameterValues);
    }
}
