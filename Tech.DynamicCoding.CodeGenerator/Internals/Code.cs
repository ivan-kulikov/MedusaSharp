﻿using System;
using System.Reflection;

namespace Tech.DynamicCoding.Internals
{
    internal class Code<TResult> : IDynamicCode<TResult>
    {
        internal Code(Delegate aDelegate)
        {
            _delegate = aDelegate;
        }

        public TResult Execute(params object[] parameterValues)
        {
            try
            {
                return (TResult)_delegate.DynamicInvoke(parameterValues);
            }
            catch (TargetInvocationException x)
            {
                throw x.InnerException;
            }
        }


        private readonly Delegate _delegate;        
    }
}
